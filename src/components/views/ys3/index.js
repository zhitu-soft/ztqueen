import flowLine from '../../widgets/flowline/partIndex'
import battery from "@/components/widgets/waterPool";
import waterPool from "@/components/widgets/waterPool/partIndex"
import watertower from "@/components/widgets/watertower/partIndex";
import elemachine from "@/components/widgets/elemachinery/partIndex";
import gate from "@/components/widgets/gate/partIndex";

export default {
    components: {flowLine, battery, waterPool, watertower, elemachine, gate},
    name: 'yanshi-3',
    data(){
        return{
            title:"组件拖拽演示",
        }
    },
    methods :{
        minExe(){
            window.require('electron').remote.getCurrentWindow().minimize();
        },
        rollback(){
            this.$router.push("/")
        }
    }

}