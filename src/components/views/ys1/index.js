import cbutton from "@/components/widgets/button/partIndex.vue";
import flowLine from '../../widgets/flowline/partIndex'
import battery from "@/components/widgets/waterPool";
import waterPool from "@/components/widgets/waterPool/partIndex"
import watertower from "@/components/widgets/watertower/partIndex";
import elemachine from "@/components/widgets/elemachinery/partIndex";
import gate from "@/components/widgets/gate/partIndex";
import * as echarts from "echarts";


export default {

    name: 'yanshi-1',
    components: {watertower, battery, elemachine, gate, waterPool, flowLine, cbutton},
    data(){
        return{
            title:"水塔水位组态演示",

            tableVisable: false,

            towerlevel: 100,
            poollevel:100,

            machineStatus: false,
            gateStatus:false,
            startStatus:false,

            noticeData: [],  //报警记录

            towerhistoryLevel : [],  // 历史水位记录， {时间与水位}
            waterhistoryLevel : [],  //历史记录
            chart: null,

            timer:null,
            isadd:false, // 水塔是否加水？
            isadd2:false,

            lineVisable: false,

        }
    },
    mounted() {

        this.$refs.flowline1.flow = this.machineStatus;
        this.$refs.flowline1.c_height = "500";
        this.$refs.flowline1.c_points = "90,452 80,452 80,40 10,40 10,180";

        this.$refs.flowline2.flow = this.machineStatus;
        this.$refs.flowline2.c_points = "100,200 100,18 40,18";

        this.$refs.flowline3.flow = this.gateStatus;
        this.$refs.flowline3.c_points = "180,20 10,20 10,200";
        //
        this.$refs.flowline4.flow = this.gateStatus;
        this.$refs.flowline4.c_points = "100,100 100,20 20,20";

        this.$refs.waterpool1.drawLevel(this.poollevel);
        this.$refs.watertower1.drawLevel(this.towerlevel);

        this.$refs.gate1.on = this.gateStatus;
        this.$refs.elemachine1.on = this.machineStatus

        this.$refs.button1.text = "启动"
        this.$refs.button1.on = this.startStatus
        this.$refs.button2.text = "复位"
        this.$refs.button3.text = "记录"

    },
    methods :{
        showLineW(){
            this.lineVisable = true
            let this_ = this;
            let x_axis = [];
            let y_axis = [];
            for(let i = 0; i < this_.waterhistoryLevel.length; i++){
                x_axis.push(this_.waterhistoryLevel[i]["date"]);
                y_axis.push(this_.waterhistoryLevel[i]["level"]);
            }
            this.showLine("水池水位历史记录", x_axis, y_axis);
        },
        showLineT(){
            this.lineVisable = true
            let this_ = this;
            let x_axis = [];
            let y_axis = [];
            for(let i = 0; i < this_.towerhistoryLevel.length; i++){
                x_axis.push(this_.towerhistoryLevel[i]["date"]);
                y_axis.push(this_.towerhistoryLevel[i]["level"]);
            }
            this.showLine("水塔水位历史记录", x_axis, y_axis);
        },
        showLine(title, x_axis, y_axis){
            let this_ = this;
            this.$nextTick(()=>{
                this_.chart = echarts.init(document.getElementById("waterlevel"), "dark");
                this_.chart.setOption({
                    title: {
                        text: title
                    },
                    tooltip: {},
                    toolbox: {
                        show: true,
                        feature: {
                            dataZoom: {
                                yAxisIndex: 'none'
                            },
                            dataView: { readOnly: false },
                            magicType: { type: ['line', 'bar'] },
                            saveAsImage: {}
                        }
                    },
                    xAxis: {
                        type: 'category',
                        data: x_axis
                    },
                    yAxis: {
                        type: 'value'
                    },
                    series: [
                        {
                            name: '水位记录',
                            type: 'line',
                            data: y_axis,
                            smooth: true
                        }
                    ]
                });
            })
        },
        minExe(){
            window.require('electron').remote.getCurrentWindow().minimize();
        },
        reset(){
            this.$router.go(0);
        },
        rollback(){
            this.$router.push("/")
        },
        recordNotice(preTowLevel, towlevel, preWaterLevel, waterLevel){
            this.towerhistoryLevel.push({"date":new Date().toLocaleTimeString(), "level": towlevel})
            this.waterhistoryLevel.push({"date":new Date().toLocaleTimeString(), "level": waterLevel})
            //正常区域
            if((preTowLevel > 80 && towlevel <= 80) || (preTowLevel < 20 && towlevel >= 20)){
                this.noticeData.push({"date":new Date().toLocaleTimeString(), "message": "水塔进入正常水位", "level":"提示"})
            }
            if((preWaterLevel > 80 && waterLevel <= 80) || (preWaterLevel < 20 && waterLevel >= 20)){
                this.noticeData.push({"date":new Date().toLocaleTimeString(), "message": "水池进入正常水位", "level":"提示"})
            }
            //
            if((preTowLevel < 80 && towlevel >= 80)){
                this.noticeData.push({"date":new Date().toLocaleTimeString(), "message": "水塔进入高水位", "level":"警告"})
            }
            if((preWaterLevel < 80 && waterLevel >= 80)){
                this.noticeData.push({"date":new Date().toLocaleTimeString(), "message": "水池进入高水位", "level":"警告"})
            }
            //
            if((preTowLevel > 20 && towlevel <= 20)){
                this.noticeData.push({"date":new Date().toLocaleTimeString(), "message": "水塔进入低水位", "level":"警告"})
            }
            if((preWaterLevel > 20 && waterLevel <= 20)){
                this.noticeData.push({"date":new Date().toLocaleTimeString(), "message": "水池进入低水位", "level":"警告"})
            }
        },
        setMachineStatus(status){
            this.machineStatus = status;
            this.$refs.elemachine1.on = status
            this.$refs.flowline2.flow = status;
            this.$refs.flowline1.flow = status;
        },
        setGateStatus(status){
            this.gateStatus = status;
            this.$refs.gate1.on = status;
            this.$refs.flowline3.flow = status;
            this.$refs.flowline4.flow = status;
        },
        changeButtonStatus(status){
            let t = "启动"
            if(status){
                t = "停止"
            }
            this.$refs.button1.text = t;
            this.$refs.button1.on = status
        },
        changeServerStatus(){
            if(this.startStatus){
                this.stop()
            } else {
                this.start()
            }
        },
        start(){
            this.startStatus = true;
            this.changeButtonStatus(true)

            let this_ = this;
            this.timer = setInterval(function (){
                if(this_.towerlevel > 20 && !this_.isadd){  // 水塔 > 20 不断的减少水位
                    this_.setMachineStatus(false);
                    let pretl = this_.towerlevel;
                    this_.towerlevel = this_.towerlevel - 1;
                    if(this_.isadd2){
                        //判断水池的水位
                        let prepl = this_.poollevel;
                        this_.poollevel = this_.poollevel + 2;
                        if(this_.poollevel > 95){
                            this_.isadd2 = false;
                            this_.setGateStatus(false);
                        }
                        this_.recordNotice(pretl, this_.towerlevel, prepl, this_.poollevel);
                        this_.$refs.waterpool1.drawLevel(this_.poollevel)
                    }
                    this_.recordNotice(pretl, this_.towerlevel, this_.poollevel, this_.poollevel);
                    this_.$refs.watertower1.drawLevel(this_.towerlevel)

                }else{
                    this_.isadd = true; //开始加水
                    //水塔水位低于， 开启状态
                    if(this_.poollevel > 20){
                        let pretl = this_.towerlevel;
                        this_.towerlevel = this_.towerlevel + 2;

                        if(this_.towerlevel > 95){
                            this_.isadd = false;
                            this_.setMachineStatus(false)
                        }

                        let prepl = this_.poollevel;
                        this_.poollevel = this_.poollevel - 2;
                        this_.recordNotice(pretl, this_.towerlevel, prepl, this_.poollevel);
                        this_.setMachineStatus(true)
                        this_.$refs.watertower1.drawLevel(this_.towerlevel)
                        this_.$refs.waterpool1.drawLevel(this_.poollevel)
                    }else{
                        this_.isadd2 = true; // 水池开始增加水位
                        let pretl = this_.towerlevel;
                        this_.towerlevel = this_.towerlevel + 2;

                        if(this_.towerlevel > 95){
                            this_.isadd = false;
                            this_.setMachineStatus(false)
                        }

                        let prepl = this_.poollevel;
                        this_.poollevel = this_.poollevel + 2;
                        if(this_.poollevel > 95) {
                            this_.isadd2 = false;
                            this.setGateStatus(false);
                        }
                        this_.setGateStatus(true);
                        this_.recordNotice(pretl, this_.towerlevel, prepl, this_.poollevel);
                        this_.$refs.watertower1.drawLevel(this_.towerlevel)
                        this_.$refs.waterpool1.drawLevel(this_.poollevel)
                    }

                }
            }, 200);
        },
        stop(){
            this.startStatus = false;
            this.changeButtonStatus(false)
            clearInterval(this.timer)
            this.timer = null;
        }
    },

}