import flowLine from '../../widgets/flowline/partIndex'
import battery from "@/components/widgets/waterPool";
import waterPool from "@/components/widgets/waterPool/partIndex";
import watertower from "@/components/widgets/watertower/partIndex";
import elemachine from "@/components/widgets/elemachinery/partIndex";
import gate from "@/components/widgets/gate/partIndex";
import cbutton from "@/components/widgets/button/partIndex";
import { ElMessage } from 'element-plus'
const path = window.require('path')
const fs = window.require('fs')
const { dialog, app} = window.require('electron').remote
import * as echarts from "echarts";
import {remote} from "electron";


export default {
    components: {flowLine, battery, waterPool, watertower, elemachine, gate, cbutton},
    name: 'yanshi-2',
    data(){
        return{
            title:"Python脚本上传演示",
            line1:true,

            kqstatus:false,         //开启状态
            waterlevel:0,

            startTimer:null,
            visible: false,
            tableVisable: false,
            lineVisable: false,

            pythonDir: (process.env.NODE_ENV !== 'production') ? (path.resolve("code")) : path.join(path.dirname(app.getPath("exe")), 'code'),

            //报警记录
            noticeData: [],
            historyLevel : [],  // 历史水位记录， {时间与水位}

            chart: null,

            sendflag: true,

        }
    },
    mounted() {

        remote.ipcMain.on('return-result', this.processPython);

        this.$refs.flowline1.flow = false;
        this.$refs.flowline1.c_points = "10,50 180,50 180,80";

        this.$refs.flowline2.c_points = "0,80 10,80 10,50 180,50 180 150";
        this.$refs.flowline2.flow = false;
        this.$refs.flowline3.flow = false
        this.$refs.flowline3.c_points = "10,150 10,50 180,50 180 200";
        this.$refs.kqbutton1.on = this.kqstatus;
        this.$refs.kqbutton1.text = '开启';

        this.$refs.kqbutton2.on = false;
        this.$refs.kqbutton2.text = '重置';

        this.$refs.kqbutton3.on = false;
        this.$refs.kqbutton3.text = '报警记录';

        this.$refs.kqbutton4.on = false;
        this.$refs.kqbutton4.text = '帮助';

        this.$refs.gate1.on = false;

        this.$refs.waterpool1.drawLevel(this.waterlevel);

    },
    methods :{
        showLine(){
            this.lineVisable = true
            let this_ = this;
            let x_axis = [];
            let y_axis = [];
            for(let i = 0; i < this_.historyLevel.length; i++){
                x_axis.push(this_.historyLevel[i]["date"]);
                y_axis.push(this_.historyLevel[i]["level"]);
            }

            this.$nextTick(()=>{
                this_.chart = echarts.init(document.getElementById("waterlevel"), "dark");
                this_.chart.setOption({
                    title: {
                        text: '历史水位示意图'
                    },
                    tooltip: {},
                    toolbox: {
                        show: true,
                        feature: {
                            dataZoom: {
                                yAxisIndex: 'none'
                            },
                            dataView: { readOnly: false },
                            magicType: { type: ['line', 'bar'] },
                            saveAsImage: {}
                        }
                    },
                    xAxis: {
                        type: 'category',
                        data: x_axis
                    },
                    yAxis: {
                        type: 'value'
                    },
                    series: [
                        {
                            name: '水位记录',
                            type: 'line',
                            data: y_axis,
                            smooth: true
                        }
                    ]
                });
            })
        },
        minExe(){
            window.require('electron').remote.getCurrentWindow().minimize();
        },
        rollback(){
            this.$router.push("/");
        },
        reset(){

            if(this.startTimer !== null){
                clearInterval(this.startTimer);
            }
            let p = path.join(this.pythonDir, "yanshi2.py");
            if(fs.existsSync(p)){
                fs.unlink(p, function (err){
                    if(!err){
                        ElMessage({
                            message: '重置成功.',
                            type: 'success',
                        });
                    }
                })
            }
            this.$router.go(0);
        },
        start(){
            let this_ = this;
            let p = path.join(this.pythonDir, "yanshi2.py");
            if(fs.existsSync(p)){
                ElMessage({
                    message: '开始调用控制文件.',
                    type: 'success',
                });
                this_.startWithPy(p)
            }else{
                this.$refs.kqbutton1.on = true;
                this.$refs.kqbutton1.text = "停止";
                // 开始流动
                this.$refs.flowline1.flow = true;
                this.$refs.flowline2.flow = true;
                this.$refs.flowline3.flow = true;
                this.$refs.gate1.on = true;
                this_.startTimer =  setInterval(function (){
                    let prelevel = this_.waterlevel;
                    this_.waterlevel = prelevel + 1;
                    if((prelevel < 20 && this_.waterlevel >= 20) || (prelevel > 80 && this_.waterlevel <= 80)){  //水位进入正常水位， 级别 正常
                        this_.noticeData.push({"date":new Date().toLocaleTimeString(), "message": "进入正常水位区域", "level":"正常"})
                    }
                    if(prelevel > 20 && this_.waterlevel <= 20){  //水位进入正常水位， 级别 正常
                        this_.noticeData.push({"date":new Date().toLocaleTimeString(), "message": "进入低水位区域", "level":"警告"})
                    }
                    if(prelevel < 80 && this_.waterlevel >= 80){  //水位进入正常水位， 级别 正常
                        this_.noticeData.push({"date":new Date().toLocaleTimeString(), "message": "进入高水位区域", "level":"警告"})
                    }
                    if(this_.waterlevel >=95){
                        this_.noticeData.push({"date":new Date().toLocaleTimeString(), "message": "水池即将溢出，强制停机", "level":"紧急"})
                        this_.stop();
                    }
                    //记录信息到本地
                    this_.historyLevel.push({"date":new Date().toLocaleTimeString(), "level": this_.waterlevel})

                    this_.$refs.waterpool1.drawLevel(this_.waterlevel)
                }, 200)
            }
        },
        processPython(event, arg){
            if(!this.sendflag){
                this.sendflag = true;
            }
            console.log(event, arg)
            let this_ = this;
            let res = arg[0]
            if(res === "code:-1"){
                clearInterval(this_.startTimer)
                ElMessage({
                    message: '执行参数异常',
                    type: 'error',
                });
            }
            if(res === "code:-2"){
                ElMessage({
                    message: '液位异常警告，关闭',
                    type: 'error',
                });
                this_.stop();
            }
            if(res.includes("level")){
                //正常的叠加液位
                let l = res.trim().split(":")[1]
                let prelevel = this_.waterlevel;
                this_.waterlevel = prelevel + parseInt(l);

                /////////////
                if((prelevel < 20 && this_.waterlevel >= 20) || (prelevel > 80 && this_.waterlevel <= 80)){  //水位进入正常水位， 级别 正常
                    this_.noticeData.push({"date":new Date().toLocaleTimeString(), "message": "进入正常水位区域", "level":"正常"})
                }
                if(prelevel > 20 && this_.waterlevel <= 20){  //水位进入正常水位， 级别 正常
                    this_.noticeData.push({"date":new Date().toLocaleTimeString(), "message": "进入低水位区域", "level":"警告"})
                }
                if(prelevel < 80 && this_.waterlevel >= 80){  //水位进入正常水位， 级别 正常
                    this_.noticeData.push({"date":new Date().toLocaleTimeString(), "message": "进入高水位区域", "level":"警告"})
                }
                if(this_.waterlevel >=95){
                    this_.noticeData.push({"date":new Date().toLocaleTimeString(), "message": "水池即将溢出，强制停机", "level":"紧急"})
                    // this_.stop();
                    this_.waterlevel = Math.floor(Math.random()*10) + 10;
                }
                //记录信息到本地
                this_.historyLevel.push({"date":new Date().toLocaleTimeString(), "level": this_.waterlevel})

                this_.$refs.waterpool1.drawLevel(this_.waterlevel)

            }
            console.log(res);
        },
        getcomputerWindows(){
            let allwindows = remote.BrowserWindow.getAllWindows()
            for(let i = 0; i < allwindows.length; i++){
                if(allwindows[i].getSize()[0] < 150){
                    return allwindows[i];
                }
            }
            return null;
        },
        startWithPy(py){
            let this_ = this;

            this.$refs.kqbutton1.on = true;
            this.$refs.kqbutton1.text = "停止";
            // 开始流动
            this.$refs.flowline1.flow = true;
            this.$refs.flowline2.flow = true;
            this.$refs.flowline3.flow = true;
            this.$refs.gate1.on = true;
            let cw = this_.getcomputerWindows()
            this.startTimer = setInterval(function (){
                if(cw != null){
                    if(this_.sendflag){
                        console.log("开始发送消息");
                        cw.webContents.send("executePython", {"level": this_.waterlevel, "python": py, "status": this_.kqstatus})
                        this_.sendflag = false;
                    }
                }
            }, 1000)

        },
        stop(){
            this.$refs.kqbutton1.on = false;
            this.$refs.kqbutton1.text = "开启";

            this.$refs.flowline1.flow = false;
            this.$refs.flowline2.flow = false;
            this.$refs.flowline3.flow = false;
            this.$refs.gate1.on = false;
            clearInterval(this.startTimer);
            this.startTimer = null;
        },
        kqserver(){
            if(this.kqstatus){
                this.stop();
                this.kqstatus = false;
            }else{
                this.start();
                this.kqstatus = true;
            }
        },
        uploadfile(){
            let this_ = this;
            dialog.showOpenDialog({
                title: "上传控制文件",
                filters: [{
                    name: 'py',
                    extensions: ['py']
                }],
                properties: ['openFile']
            }).then(result => {
                if(result === undefined || result === "" || result == null || result.canceled || result.filePaths.length === 0){
                    console.log("控制文件选择有误")
                    ElMessage({
                        message: '控制文件选择有误.',
                        type: 'warning',

                    });
                }else{

                    if(!fs.existsSync(this_.pythonDir)){
                        fs.mkdirSync(this_.pythonDir)          //创建出这个目录
                    }
                    fs.copyFile(result.filePaths[0], path.join(this_.pythonDir, "yanshi2.py"), function (err){
                        if(err){
                            ElMessage({
                                message: '上传控制脚本失败.',
                                type: 'warning',
                            });
                        }else{
                            ElMessage({
                                message: '上传控制脚本成功.',
                                type: 'success',
                            });
                        }

                    })
                }
            })

        }
    },
    beforeDestroy(){
        remote.ipcMain.removeListener('return-result', this.processPython);
    }

}