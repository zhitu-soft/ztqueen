
const ipc = window.require('electron').ipcRenderer
const execSync = window.require('child_process').execSync


ipc.on('executePython', (event, arg) =>{
    console.log(event, arg)
    let res = execSync('python ' + arg["python"] + " " + arg["level"]).toString();

    ipc.send('return-result', [res])
})



export default {
    name: 'cpmopter-index',
    props: {

    },
    data () {
        return {

        }
    },
    watch: {
    },
    computed: {
    },
    methods: {

    },
    mounted () {


    },
    created () {

    }
}
