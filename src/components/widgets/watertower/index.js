
export default {
    mycanvas: null,
    ctx: null,
    myInterval: null,
    props: {
        proQuantity: {
            type: Number,
            default: 10
        },
        proIsCharge: {
            type: Boolean,
            default: false
        }
    },
    name: 'water-pool',
    data(){
        return{
            cWidth: 200,
            cHeight: 450,
            jd: 0
        }
    },
    methods :{
        drawBg () {
            this.ctx.strokeStyle = 'rgba(0,0,0,1)'
            this.ctx.lineWidth = 4
            this.ctx.strokeRect(0, 0, 1, 188)
            this.ctx.strokeRect(0, 188, 180, 2)
            this.ctx.strokeRect(180, 0, 1, 188)
            this.ctx.fillStyle = 'rgba(255, 255, 255, 0.3)'
            this.ctx.fillRect(0, 0, 180, 188)
            // 下边画一个墙
            var image = new Image();
            image.src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/4QBGRXhpZgAATU0AKgAAAAgABAESAAMAAAABAAEAAFEQAAEAAAABAQAAAFERAAQAAAABAAAAAFESAAQAAAABAAAAAAAAAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCADwAJYDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwDzf/hYv7Y//RLPhB/4US//ACRXHfH39rv9qb9mf4T6l408VfDP4VWug6U8MdxJbaybiVTLIsaYRJyx+Zh0HA9q+19vtXzD/wAFkh/xro8c/wDX1pn/AKXRV4VGpGU1FxW/n/mfaYmjUp0pVFUldK/T/Iqw/Er9sa5gilX4W/CHbLGsi58RDkMAR/y8ehp3/CxP2xwf+SWfCD/wol/+P19PaUP+JRY/9esP/otanA4qPbL+Vfj/AJm/1WbX8SX3r/I+Wv8AhY37Y/8A0S34Qf8AhRL/APH6D8RP2x/+iWfCD/wol/8Akivqbb7Ubfal7Zfyr8f8xfVJ/wDPyX3r/I+Wf+Fi/tj/APRLPg//AOFEv/yRR/wsX9sc/wDNLPhB/wCFEv8A8fr6m2+1G0+lHtl/Kvx/zD6rP/n5L71/kfLP/CxP2x/+iW/CD/wol/8AkivO/hv8Nv2tPhp8fviP8QrX4e/DC41L4lfYvtttN4jj8i0+yxmNPLxNu5BydxPtivunYc0u0044i2iivx/zJlgW2m6ktNtvTsfLP/CxP2x/+iWfCD/wol/+SKB8Rf2x/wDolvwg/wDCiX/5Ir6m2+1G32pe2X8q/H/Mr6pP/n5L71/kfLP/AAsX9sf/AKJZ8IP/AAol/wDkij/hYv7Y/wD0Sz4Qf+FEv/yRX1Nt9qNvtR7Zfyr8f8w+qT/5+S+9f5Hyz/wsX9sf/olnwg/8KJf/AJIoHxF/bHH/ADSz4Qf+FEv/AMkV9Tbfajb7Ue2X8q/H/MPqk/8An5L71/kfEnxz/bG/ai/Z10TQtQ8UfDT4V29r4i1u30CyNvrJuC93OHaNWCznapEbZY8DHNdxN8QP2ybaZ42+Fnwf3RsVOPEa9Rx/z8VU/wCCuI/4tT8Jf+yqaN/6Kuq+tNV51W595n/9CNayqR5Iy5Vrfv8A5mNOlUdWVP2ktLdV1+R8pn4iftj/APRLPhB/4US//H6K+ptvtRWftl/Kvx/zN/qs/wDn5L71/kOr5f8A+Cyn/KOnx1/19aX/AOl0VfUFfL//AAWU/wCUdPjr/r60v/0uiqcP/Fj6lY//AHafoz6Y0n/kE2P/AF6Q/wDotanXp+JqDSf+QTY/9ekP/otanXp+JrHqzqjsLRRRQMKKKKACiiigAooooAKKKKACiiigD5R/4K4/8kr+Ev8A2VXRv/RV1X1lqf8AyFbj/rs/8zXyb/wVx/5JX8Jf+yq6N/6Kuq+stT/5Ctx/12f+ZraX8KPz/Q46P+81Pl+RHRRRWJ2Bur5f/wCCyh/410+Ov+vrS/8A0uir6S/tvT/+f/T/APwJj/xr5l/4LF6paXP/AATt8cJDd2crm60zCxzozH/TYuwNdGHi/ax06nDjqkfq89Vsz6f0lv8AiU2P/XpD/wCi1qdTx+JrO0zW7EaTY/6dYD/RYeDcp/zzX3qX+29P/wCf/T//AAJj/wAax5XfY6o1I23Re3Ubqpf23p//AD/af/4Ex/40n9taf/z/AOn/APgTH/jRyy7B7SPdF7dRuqj/AG3p/wDz/wCn/wDgTH/jR/ben5/4/wDT/wDwJj/xo5Zdg9pHui9mjNUTrtj/AM/9h/4Ex/40f27Yf9BDT/8AwJj/AMaXK+xXtI9y9uo3VR/trT/+f/T/APwJj/xo/tvT/wDn+0//AMCY/wDGnyy7E+0j3Re3Ubqo/wBt6f8A8/2n/wDgTH/jR/ben/8AP/p//gTH/jRyy7B7SPdF7dRuqj/ben/8/wBp/wD4Ex/40f23p/8Az/6f/wCBMf8AjRyy7B7SPdHy/wD8FcTn4VfCX/squjf+irqvrLVONVuP+uz/AMzXyL/wVr1O1ufhX8JljurWXb8U9GY7JlbAEV1k8HoM9fevrDVdc086pc/8TCw/1z4/0lPU+9bTi/ZR07/octGUfrFR3/l/Im3UVR/trT/+f/T/APwJj/xorHll2Or2ke6PnP8A4c9/s+f9CTdf+Def/Ggf8Ee/2fP+hJuv/BtP/jX01RWn1ir/ADMx+o4f+RfcfMv/AA56/Z7/AOhJuv8Awbz/AONH/Dnv9nv/AKEm6/8ABvP/AI19NUUe3qfzP7w+o4f+Rfcj5l/4c9/s+f8AQk3X/g3n/wAaP+HPf7Pn/Qk3X/g3n/xr6aoo+sVf5mH1HD/yL7j5l/4c9/s+f9CTdf8Ag3n/AMaP+HPf7Pn/AEJN1/4N5/8AGvpqij6xV/mYfUcP/IvuPmM/8Eff2e8N/wAUTdf+Def/ABrwX9nz/gnL8H/HH7cfx/8AB+p+GLi48PeCf7H/ALGthqMqG18+AvLlgctlh36V+iZ+631P8q+W/wBlD/lJj+1T/wBy/wD+kpralXqOMm29v8jjxGEoKpStBavXTyZd/wCHPf7Pn/Qk3X/g3n/xo/4c9/s+f9CTdf8Ag3n/AMa+mqKx+sVf5mdn1HD/AMi+4+Zf+HPf7Pn/AEJN1/4N5/8AGj/hz3+z5/0JN1/4N5/8a+mqKPrFX+Zh9Rw/8i+4+Zf+HPf7Pn/Qk3X/AIN5/wDGj/hz3+z5/wBCTdf+Def/ABr6aoo+sVf5mH1HD/yL7j5l/wCHPf7PY/5km6/8G8/+NH/Dnv8AZ7x/yJN1/wCDef8Axr6aoo+sVP5n94fUcP8AyL7kfMv/AA57/Z8/6Em6/wDBvP8A40V9NUUfWKv8zD6jh/5F9wUUUVidQUUUUAFFFFABRRRQAw/db6n+VfLf7KH/ACkx/aq/7l//ANJTX1Ifut9T/Kvlv9lD/lJj+1V/3L//AKSmt6PwT9P1Rw4r+JR9f0Z9T0UUVgdwUUUUAFFFFABRRRQAUUUUAFFfJ3/Devxw/wCjQPiR/wCDc/8AyFWD8TP+CofxP+DXgq68SeKv2WfHHh/QbFo0uL+91zyoIS7BEDMbPA3MQB7muhYWo/8Ah0cTzCild3+5/wCR9n0V8mp+3t8cJIkdf2QfiQyyKrqRq7YYEZB/48+4NL/w3p8cB/zaB8SP/Buf/kOl9Xqf01/mP69R8/uf+R9Y0V8nf8N6/HD/AKNA+JH/AINz/wDIVH/Devxw/wCjQPiR/wCDc/8AyFT+rVP6aD69S8/uf+R9Y0V8nf8ADevxw/6NA+JH/g3P/wAhUf8ADevxw/6NA+JH/g3P/wAhUfVqn9NB9epef3P/ACPq4/db6n+VfLf7KH/KTH9qn/uX/wD0lNVP+G8vjgf+bP8A4kc/9Rdv/kOvKfhJ8Xvjp8L/ANp/4sfEVv2WfiJfR/Ez+ztmni7aJtN+yxbOZfsx8zfnP3Ex71pToyUZJ21815HLiMVCU6bV9Hro+zXY/Qiivk7/AIb1+OH/AEaB8SP/AAbn/wCQqP8AhvX44f8ARoHxI/8ABuf/AJCrP6tU/po6vr1Lz+5/5H1jRXyd/wAN6/HD/o0D4kf+Dc//ACFR/wAN6/HD/o0D4kf+Dc//ACFR9Wqf00H16l5/c/8AI+saK+Tv+G9fjh/0aB8SP/Buf/kKj/hvX44f9GgfEj/wbn/5Co+rVP6aD69S8/uf+R9Y0V8W/Eb/AIKk/Ez4Rafp114o/Zb8caDbaxqEWk2Ut5rhjW5u5AxjgUmz5dgrEDvg10cn7eHxyhkZW/Y++JKspIIOrNwf/AOj6tU/pr/MP7Qo3td/c/8AI+r6K+Tv+G9fjh/0aB8SP/Buf/kKij6tU/poPr1Lz+5/5H1XtX+7XzH/AMFkFx/wTo8c8f8AL1pnb/p+ir6ir5f/AOCyn/KOnx1/19aX/wCl0VTh/wCLH1DHf7vP0Z9LaWoOj2PH/LrD/wCi1qTZ/s03Sf8AkE2P/XpD/wCi1qden4msup1R2Itq/wB2jaP7tTUUh2Itq/3aTaP7tTUUAR+Xk0GLjtUlFAyHaP7tG0f3amooEQ7V/u0bR/dqaigLEO1f7tG1f7tTUUAfJ/8AwVwA/wCFVfCX/sqmi/8Aoq6r6z1Yf8TS54H+uf8A9CNfJ3/BXH/klfwl/wCyq6N/6Kuq+stT/wCQtcf9dX/ma2n/AAo/P9Dkpf7xU/7d/IrbR/doqWisTsCvl/8A4LKf8o6fHX/X1pf/AKXRV9QV8v8A/BZT/lHT46/6+tL/APS6KtsP/Fj6nJj/APdp+jPpjSf+QTY/9ekP/otanXp+JqDST/xKbH/r0h/9FrU69PxNY9WdUdhaKKKBhRRRQAUUUUAFFFFABRRRQAUUUUAfKP8AwVx/5JX8Jf8Asqujf+irqvrLU/8AkK3H/XZ/5mvk3/grj/ySv4S/9lV0b/0VdV9Zan/yFbj/AK7P/M1tL+FH5/ocdH/eany/IjooorE7B32K4/54z/8AfBr5f/4LK2ssf/BObx0zRyKv2rS+WUj/AJfoqpD/AIJNeGf+irfHL/wpv/sKpeIf+CO/gnxbo82n6t8RvjLqdhMVMltd+IFmhkKnK5RoyDggEZHUV1U/ZRmpc34Hm4j6zUpSp8m6tuj6z0uwnOkWP7mb/j1h/gP/ADzWpxY3H/PGb/vg18or/wAEmvDKhVX4q/HIBQAMeJ+gH/AKP+HTXho/81V+OX/hTf8A2FRyUv5vwNvaYm1vZ/ij6u+xXH/PGf8A74NH2G4/54z/APfBr5T/AOHTXhn/AKKt8cv/AAp//sKb/wAOmvDP/RVvjl/4U3/2FHLS/m/D/gh7XEf8+/xR9XfYrj/njP8A98Gj7Dcf88Z/++DXyn/w6a8M/wDRVvjl/wCFP/8AYUn/AA6a8M/9FW+OX/hTf/YUctL+b8P+CHtcR/z7/FH1b9hn/wCeM3/fBoFhcf8APGb/AL5NfKLf8EnPDIH/ACVb45f+FN/9hXi/wQ/YitfiP+2F8bfAN/8AE74wR6L8Ov7L/syWHxGwuJftMJkk80kEHB6bQOOuauNGEk2pbeX/AATOWKrxkk6fxeflc/Rb7FP/AM8Z/wDvg0fYbj/njP8A98GvlL/h014Zx/yVb45f+FN/9hR/w6a8M/8ARVvjl/4U3/2FRy0v5vw/4Jp7XEf8+/xR9W/Yrj/njP8A98Gj7Dcf88Z/++DXyl/w6a8M/wDRVvjl/wCFN/8AYUn/AA6a8M/9FW+OX/hTf/YUctL+b8P+CHtcR/z7/FH1d9iuP+eM/wD3waPsVx/zxn/74NfKf/Dprwz/ANFW+OX/AIU//wBhSD/gk14Z/wCirfHL/wAKb/7Cjlpfzfh/wQ9riP8An3+KE/4K52k0fwp+EpaORd3xV0UDKnk+VdV9batYTf2rdfuZv9c/8B/vGvjzXv8Agjp4H8VQW8WqfEX4yalHazrdQpda+sywyrnbIoaM7XGThhzyelX2/wCCTnhtmyfit8cmYnk/8JP1/wDHK0fsnBR5tvIxh9ZVSVTkWtuq6H1Z9iuP+eM//fBor5RP/BJrwz/0VX45f+FN/wDYUVnyUv5vw/4Jt7XEf8+/xR9WUUUVgdgUUUUAFFFFABRRRQAw/db6n+VfLf7KH/KTH9qn/uX/AP0lNfUh+631P8q+W/2UP+UmP7VX/cv/APpKa3o/BP0/VHDiv4lH1/Rn1PRRRWB3BRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAMP3W+p/lXy3+yh/ykx/ap/wC5f/8ASU19SH7rfU/yr5b/AGUP+UmP7VX/AHL/AP6Smt6PwT9P1Rw4r+JR9f0Z9T0UUVgdwUUUUAFFFFABRRRQAUUUUAfJv/D474d/9E7+Pn/hI23/AMm1V1j/AILU/C/w7p0l5qHgX452NpEQHnuPC1pHGmTgZZr4AZPFfYP9tXn/AD93n/f9v8a+YP8Agsvqlzc/8E5PHSSXFxIhu9L+V5CwP+nRe9ddP2UpKPK9fM83EfWadKVTn2V9jG/4fH/DsqD/AMK7+PmGGR/xSNtyP/A2l/4fHfDz/onfx8/8JG2/+Ta+sdL1q8GkWOLy7/49If8Als3H7tfep/7avP8An7vP+/zf41HNS25X95t7HE2vz/gfI/8Aw+O+Hf8A0Tv4+f8AhI23/wAnUn/D474d/wDRO/j5/wCEjbf/ACbX1x/bV5/z93n/AH/b/Gj+2rz/AJ+7z/v+3+NHNS/kf3i9lif5/wDyU+SP+Hx3w7/6J38fP/CRtv8A5No/4fHfDv8A6J38fP8Awkbb/wCTa+t/7avP+fu8/wC/7f40f21ef8/d5/3/AG/xo5qX8j+8PZYn+f8A8lPkY/8ABYz4d4P/ABbv4+c/9Sja/wDybXivwO/4KC+G/hv+2J8bvH+o+AfjJJoXxG/sr+yY7bwzE93F9mhKSeejXKomSfl2O+R1x0r9I/7avP8An7vP+/7f40f23ef8/d5/3/b/ABqo1aaTShv5mcsLXk4yc/h20+R8j/8AD474d/8ARO/j5/4SNt/8m0v/AA+O+Hf/AETv4+f+Ejbf/J1fW/8AbV5/z93n/f8Ab/Gj+2rz/n7vP+/7f41PNS/kf3mnssT/AD/+SnyR/wAPjvh3/wBE7+Pn/hI23/ybSf8AD474d/8ARO/j5/4SNt/8m19cf21ef8/d5/3/AG/xo/tq8/5+7z/v+3+NHNS/kf3h7LE/z/8Akp8kf8Pjvh3/ANE7+Pn/AISNt/8AJtH/AA+O+Hf/AETv4+f+Ejbf/J1fW/8AbV5/z93n/f8Ab/Gj+2rz/n7vP+/zf40c1L+R/eHssT/P/wCSnx7q3/Bav4X6BDDJfeBvjlZR3EqwRNP4WtIxJI2dqKWvhljg4A5ODVs/8Fjfh6GIPw6+Poxwf+KQtuP/ACdq1/wV21O6uPhT8I/MuLiTb8VtFZQ0pbB8q65FfWura3ef2pdf6Zef65/+Wzf3j71pL2Sipcr18zKKxLqSp8+1unc+P/8Ah8f8PP8Aonfx8/8ACRtv/k2ivrj+2rw/8vV5/wB/2/xorPmpfyfia+yxP8//AJKQbK+Yf+Cya4/4J0+Of+vrTP8A0uir6gr5f/4LKf8AKOnx1/19aX/6XRVOH/ix9R4//dp+jPpbSlzpFj/16Q/+i1qwqZFQ6T/yCbH/AK9If/Ra1OvT8TWL3Z1R2E2UbKdRQUN2UbKdRQA3ZRsp1FADdlGynUUAN2UbKdRQA3ZRsp1FAHyh/wAFcRj4V/CX/squjf8Aoq6r6z1Qf8TW4/67P/M18m/8Fcf+SV/CX/squjf+irqvrLU/+Qrcf9dn/ma2l/Cj8/0OKl/vNT5fkQ7KKdRWJ2hXy/8A8FlP+UdPjr/r60v/ANLoq+nPOr5i/wCCyT7v+CdHjj/r60z/ANLoq3w6ftY+py4//dp+jPpnSf8AkE2P/XpD/wCi1qden4mq2lP/AMSix/69If8A0WtTCXaP/rVi07s6Y7ElFR+dR51FmMkoqPzqPOoswJKKaJQf/wBVJ5tIB9FR+dR51OzAkoqPzqPOoswJKKj86jzqLMD5V/4K4/8AJK/hL/2VXRv/AEVdV9Zan/yFbj/rs/8AM18l/wDBXBs/Cr4S9P8Akqujf+irqvrTVWA1W556TP8AzNbS/hR+f6HHS/3mp/27+RHRUfnf5xRWXKzsPlv/AIYZ+NX/AEd78Rv/AAQp/wDJlYPxQ/4Ji/Ej41eCLvw34s/am8c69oN+0b3FjeeHUkhmMbh0JH2z+FgCPcV9i7BRsFa/WJrVfkv8jheXUGrNP73/AJnyvH+wp8aIY0Rf2vPiMFjUIoGgLgAAAD/j87AYp3/DDPxqH/N33xH/APBAv/yZX1NsFGwUfWJ/0l/kV/Z9Hz/8Cf8AmfLP/DDPxqz/AMnffEb/AMEC/wDyZR/wwz8av+jvviN/4IE/+TK+ptgo2Cl9Yn5fcv8AIX9n0fP73/mfLJ/YZ+NX/R3vxG/8EKf/ACZR/wAMM/Gr/o774jf+CBP/AJMr6m2CjYKPrE/L7l/kP+z6Pn97/wAz5ZP7DHxqH/N33xG/8EC//JleUfCL4W/HH4n/ALUXxb+HUn7UnxBsYfhj/Z3l6gumiVtS+1RFzmL7Qoi2Yxw77uvFffZ5DV8t/sojP/BTD9qr/uX/AP0lNbU60pRk30XZd15HLiMLTjOmo31eur7PzE/4Ya+NX/R3vxG/8EKf/JlL/wAMM/Gr/o774jf+CBP/AJMr6m2CjYKx+sT8vuX+R0/2fR8/vf8AmfLP/DDPxq/6O++I3/ggT/5Mo/4YZ+NX/R33xG/8ECf/ACZX1NsFGwUfWJ+X3L/If9n0fP73/mfLP/DDPxq/6O++I3/ggT/5MpP+GGfjV/0d78Rv/BCn/wAmV9T7BRsFH1ifl9y/yF/Z9Hz+9/5nxt8Sf+CXvxG+MGm6baeKP2pPHGuW2j6jFq9lFd+HVdba7iDCOZf9M4dQ7YPua6ST9hz42SyM7ftf/EhmYkk/2CvJPX/l8r6l20bBVfWqlrfov8hf2fRvez+9/wCZ8sf8MM/Gr/o734jf+CFP/kyivqfYKKX1ifl9y/yK/s+j5/8AgT/zP//Z";
            image.onload = () => {
                this.ctx.drawImage(image, 15, 192);
            };


        },
        // 根据电量画出一个块
        drawPath (quantity) {
            this.ctx.fillStyle = '#02ffff'
            this.ctx.fillRect(3, 188 - (quantity / 100) * 188, 175, (quantity / 100) * 188)

            this.ctx.beginPath()
            this.ctx.arc(190,150,10,0,2*Math.PI)
            this.ctx.closePath();
            if(this.jd < 20){
                this.ctx.fillStyle='red'
            }else{
                this.ctx.fillStyle = "green";
            }
            this.ctx.fill();

            this.ctx.beginPath()
            this.ctx.arc(190,40,10,0,2*Math.PI)
            this.ctx.closePath();
            if(this.jd > 80){
                this.ctx.fillStyle='red'
            }else{
                this.ctx.fillStyle = "green";
            }
            this.ctx.fill();
        },
        drawCharge () {
            this.ctx.clearRect(0, 0, this.cWidth, this.cHeight)
            this.drawBg()
            this.jd = this.jd + 10
            if (this.jd > 100) {
                this.jd = 0
            }
            this.drawPath(this.jd)
        },
        drawPath2 (quantity) {
            this.ctx.fillStyle = '#02ffff'
            this.ctx.fillRect(3, 188 - (quantity / 100) * 188, 175, (quantity / 100) * 188)


            this.ctx.beginPath()
            this.ctx.arc(190,150,10,0,2*Math.PI)
            this.ctx.closePath();
            if(quantity < 20){
                this.ctx.fillStyle='red'
            }else{
                this.ctx.fillStyle = "green";
            }
            this.ctx.fill();

            this.ctx.beginPath()
            this.ctx.arc(190,40,10,0,2*Math.PI)
            this.ctx.closePath();
            if(quantity > 80){
                this.ctx.fillStyle='red'
            }else{
                this.ctx.fillStyle = "green";
            }
            this.ctx.fill();
        },
        drawLevel (info) {
            this.ctx.clearRect(0, 0, this.cWidth, this.cHeight)
            this.drawBg()
            this.drawPath2(info)
        }
    },
    mounted () {
        this.mycanvas = this.$refs.mycanvas
        this.ctx = this.mycanvas.getContext('2d')
        this.drawBg()
        this.drawPath(this.proQuantity)
    },

}