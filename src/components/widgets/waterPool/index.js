
export default {
    mycanvas: null,
    ctx: null,
    myInterval: null,
    props: {
        proQuantity: {
            type: Number,
            default: 89
        },
        proIsCharge: {
            type: Boolean,
            default: false
        }
    },
    name: 'water-pool',
    data(){
        return{
            cWidth: 200,
            cHeight: 188,
            jd: 0
        }
    },
    methods :{
        drawBg () {
            this.ctx.strokeStyle = 'rgba(0,0,0,1)'
            this.ctx.lineWidth = 4
            this.ctx.strokeRect(0, 0, 1, 188)
            this.ctx.strokeRect(0, 188, 180, 2)
            this.ctx.strokeRect(180, 0, 1, 188)
            this.ctx.fillStyle = 'rgba(255, 255, 255, 0.3)'
            this.ctx.fillRect(0, 0, 180, 188)

        },
        // 根据电量画出一个块
        drawPath (quantity) {
            this.ctx.fillStyle = '#02ffff'
            this.ctx.fillRect(3, 188 - (quantity / 100) * 188, 175, 185)

            this.ctx.beginPath()
            this.ctx.arc(190,150,10,0,2*Math.PI)
            this.ctx.closePath();
            if(this.jd > 80){
                this.ctx.fillStyle='red'
            }else{
                this.ctx.fillStyle = "green";
            }
            this.ctx.fill();

            this.ctx.beginPath()
            this.ctx.arc(190,40,10,0,2*Math.PI)
            this.ctx.closePath();
            if(this.jd < 20){
                this.ctx.fillStyle='red'
            }else{
                this.ctx.fillStyle = "green";
            }
            this.ctx.fill();

        },
        drawPath2 (quantity) {
            this.ctx.fillStyle = '#02ffff'
            this.ctx.fillRect(3, 188 - (quantity / 100) * 188, 175, 185)


            this.ctx.beginPath()
            this.ctx.arc(190,150,10,0,2*Math.PI)
            this.ctx.closePath();
            if(quantity < 20){
                this.ctx.fillStyle='red'
            }else{
                this.ctx.fillStyle = "green";
            }
            this.ctx.fill();

            this.ctx.beginPath()
            this.ctx.arc(190,40,10,0,2*Math.PI)
            this.ctx.closePath();
            if(quantity > 80){
                this.ctx.fillStyle='red'
            }else{
                this.ctx.fillStyle = "green";
            }
            this.ctx.fill();
        },
        drawCharge () {
            this.ctx.clearRect(0, 0, this.cWidth, this.cHeight)
            this.drawBg()
            this.jd = this.jd + 10
            if (this.jd > 100) {
                this.jd = 0
            }
            this.drawPath(this.jd)
        },
        drawLevel (info) {
            this.ctx.clearRect(0, 0, this.cWidth, this.cHeight)
            this.drawBg()
            this.drawPath2(info)
        }
    },
    mounted () {
        this.mycanvas = this.$refs.mycanvas
        this.ctx = this.mycanvas.getContext('2d')
        this.drawBg()
        this.drawPath(this.proQuantity)
    },

}