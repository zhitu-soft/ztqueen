import { createRouter,createWebHashHistory } from "vue-router"

// 这里展现有哪些路由路径
const routes = [
    {
        name: '登录',
        path:'/',
        component: () => import('../components/home'),
    },
    {
        name: '演示页面1',
        path:'/ys1',
        component: () => import('../components/views/ys1/appIndex'),
    },
    {
        name: '演示页面2',
        path:'/ys2',
        component: () => import('../components/views/ys2/appIndex'),
    },
    {
        name: '演示页面3',
        path:'/ys3',
        component: () => import('../components/views/ys3/appIndex'),
    },
    {
        name: '演示页面4',
        path:'/ys4',
        component: () => import('../components/views/ys4/appIndex'),
    },
    {
        name: '计算窗口',
        path:'/computer',
        component: () => import('../components/computer/partIndex'),
    },
    {
        path:'/:catchAll(.*)',
        redirect: "/"
    },

]

// createRouter() 来创建路由
const router = createRouter({
    routes, // 路由路径哪些
    // history: createWebHistory(),
    history: createWebHashHistory()
})

// 导出这个变量
export default router
