const { defineConfig } = require('@vue/cli-service')
const NodePolyfillPlugin = require('node-polyfill-webpack-plugin')

module.exports = defineConfig({
  transpileDependencies: false,
  pluginOptions: {
      electronBuilder:{
      builderOptions: {
          appId: "com.hxyt.ztqueen",
          productName: "ZTQ",
          copyright: "hxyt © 2023",
          directories: {
              output: "./build" //输出文件路径
          },
          win: {
              //win相关配置
              icon: "build/icons/logo.png", //图标，当前图标在根目录下，注意这里有两个坑
          }
      },
    },
  },
  chainWebpack: config => {
    config.module
        .rule('images')
        .set('parser', {
          dataUrlCondition: {
            maxSize: 1024 * 1024 * 1024  // 4KiB
          }
        })
    config.module
        .rule('fonts')
        .test(/\.(woff2?|eot|ttf|otf)(\?.*)?$/)
        .set('parser', {
          dataUrlCondition: {
            maxSize: 400 * 1024 * 1024  // 4KiB
          }
        })
  },
  configureWebpack: {
    plugins: [new NodePolyfillPlugin()],
    externals: {
      'electron': 'require("electron")'
    },
    resolve: {
      fallback:{
        fs:false,
      }
    }
  },
})